# 21 Dias De Codigo
Desafio proposto pela [Rocketseat](http://rocketseat.com.br). Codar pelo menos 1 hora por dia durante 21 dias e concluir desafios diários.
<br>
[POST DO DESAFIO](https://www.instagram.com/p/ChTBg1BpLGU/)
<br>
<br>
## Desafios:
<ol>
<li>Crie um repositório para listar os projetos que serão desenvolvidos durante os 21 dias de código. <a href="https://github.com/jakeliny/21-dias-de-codigo-rocketseat">DESAFIO CONCLUÍDO</a></li>
<li> Faça um Hello World customizado com fontes, cores e imagens. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-2/">DESAFIO CONCLUÍDO</a></li>
<li>Crie um botão animado. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-3/">DESAFIO CONCLUÍDO</a></li>
<li>Crie uma calculadora. <a href="https://jakeliny.github.io/21-dias-de-codigo-rocketseat/Desafio-4/">DESAFIO CONCLUÍDO</a></li>
<li>Desenvolva uma página customizada que exiba o erro 404. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-5/">DESAFIO CONCLUÍDO</a></li>
<li>Crie um card com o efeito glassmorphism. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-6/">DESAFIO CONCLUÍDO</a></li>
<li>Desenvolva um contador. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-7/">DESAFIO CONCLUÍDO</a></li>
<li>Desenvolva um toggler que altere o tema para claro e escuro. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-8/"></a></li>
<li>Crie uma animação de loading. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-9/"></a></li>
<li>Utilize a lib Particle.js do JavaScript. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-10/"></a></li>
<li>Crie o layout de um formulário de login. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-11/"></a></li>
<li>Desenvolva uma galeria de imagens. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-12/"></a></li>
<li>Desenvolva um menu responsivo. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-13/"></a></li>
<li>Crie um formulário de quizzes. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-14/"></a></li>
<li>Desenvolva um site que exiba uma vitrine de produtos de um e-commerce fictício(com nome dos produtos,preço e imagens). <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-15/"></a></li>
<li>Desenvolva um site utilizando alguma API pública.. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-16/"></a></li>
<li>Desenvolva um site que capture os eventos do teclado e exiba na tela o evento e a tecla pressionada. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-17/"></a></li>
<li>Desenvolva um gerador de senhas aleatórias. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-18/"></a></li>
<li>Desenvolva um site que gere imagens aleatórias utilizando a API do Unsplash. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-19/"></a></li>
<li>Desenvolva um site de emoji picker. Ao selecionar o emoji, é copiado para área de transferência. <a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-20/"></a></li>
<li>Desenvolva uma página para ser seu portifólio.</br>
Conte sobre você.</br>
Suas experiências.</br>
Suas habilidads</br>
Inclua no portfólio todos desafios desenvolvidos durante esses 21 dias.</br>
<a href="https://github.com/joeltimoteo/21-dias-de-codigo/Desafio-21/"></a></li>
</ol>
